
#include <dt-bindings/zmk/trackball_pim447.h>
#include "twobytwo3.dtsi"                      // Notice that the main dtsi files are included in the overlay.

&default_transform { // The matrix transform for this board is 2 columns over because the left half is 2 columns wide according to the matrix.
  col-offset = <2>;
};

&kscan0 {
  col-gpios
    = <&pro_micro 10 (GPIO_ACTIVE_HIGH | GPIO_PULL_DOWN)>
    , <&pro_micro 16 (GPIO_ACTIVE_HIGH | GPIO_PULL_DOWN)>
    ;
};

// Note that the I2C bus for the Pimoroni 447 Track Ball can be defined in two ways. Either as &i2c0 which is
// automatically set up with pins
//
//  sda-pin = <17>; // this is read as 0*32 + 17 for P0.17 which is GPIO 2 in SparkFun Pro Micro notation.
//  scl-pin = <20>; //                 0*32 + 20     P0.20          GPIO 3
//
// for the Nice!Nano as can be seen here:
// https://github.com/zmkfirmware/zmk/blob/main/app/boards/arm/nice_nano/nice_nano.dtsi#L45

&i2c0 {
  status = "okay";

  trackball_pim447@a {
    compatible = "pimoroni,trackball_pim447";
    reg = <0xa>;
    label = "TRACKBALL_PIM447";
    mode = <PIM447_MOVE>;
    move-factor = <2>;           // Increase pointer velocity (default: 1)
  };
};

// Alternatively, I can use the notation
//
//&pro_micro_i2c {
//  status = "okay";
//
//  trackball_pim447@a {
//    ...
//  };
//};

// further properties:
//   invert-move-x;          // Invert pointer X axis (left is right, and vice versa)
//   invert-move-y;          // Invert pointer Y axis (up is down, and vice versa)
//   button = <1>;           // Send right-click when pressing the ball (default: 0, ie. left-click)
//   swap-axes;              // Swap X and Y axes (horizontal is vertical, and vice versa)
//   scroll-divisor = <1>;   // Increase wheel velocity (default: 2)
//   invert-scroll-x;        // Invert wheel X axis (left is right, and vice versa)
//   invert-scroll-y;        // Invert wheel Y axis (up is down, and vice versa)
